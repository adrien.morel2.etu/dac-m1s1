terraform {
required_version = ">= 0.13.4"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

resource "openstack_compute_keypair_v2" "test-keypair" {
  name       = "my-keypair"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "openstack_compute_instance_v2" "terraform_instances-ubuntu" {
	count = 2
	name = "instance-terraform-ubuntu-${count.index}"
	provider = openstack
	image_name = "ubuntu-20.04"
	flavor_name = "normale"
	key_pair = openstack_compute_keypair_v2.test-keypair.name
}


resource "openstack_compute_instance_v2" "terraform_instances-centos" {
	count = 2
	name = "instance-terraform-centos-${count.index}"
	provider = openstack
	image_name = "centos-7"
	flavor_name = "normale"
	key_pair = openstack_compute_keypair_v2.test-keypair.name
}


